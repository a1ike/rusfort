$(document).ready(function () {
  /* $('a[href^="#"]').on('click', function (e) {
    e.preventDefault();
    var target = this.hash,
      $target = $(target);

    $('html, body').stop().animate({
      'scrollTop': $target.offset().top
    }, 900, 'swing', function () {
      window.location.hash = target;
    });
  }); */

  $('.phone').inputmask('+7(999)999-99-99');

  $('.r-big').slick({
    dots: true,
    arrows: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 5000
  });

  $('.r-reviews__cards').slick({
    dots: true,
    arrows: true,
    infinite: true,
    speed: 500,
    slidesToShow: 2,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 5000,
    responsive: [{
      breakpoint: 1200,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
        dots: false
      }
    }]
  });

  $('.r-licenses__cards').slick({
    dots: false,
    arrows: true,
    infinite: true,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 5000,
    responsive: [{
      breakpoint: 1200,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
        dots: false
      }
    }]
  });

  $('.r-more__cards').slick({
    dots: false,
    arrows: true,
    infinite: true,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 5000,
    responsive: [{
      breakpoint: 1200,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
        dots: false
      }
    }]
  });

  $('#slider-summ').rangeslider({
    polyfill: false,

    onInit: function () {},

    onSlide: function (position, value) {
      $('#input-summ').val(value);
    },
  });

  $('#input-summ').on('input', function () {
    $('#slider-summ').val($(this).val()).change();
  });

  $('#slider-days').rangeslider({
    polyfill: false,

    onInit: function () {},

    onSlide: function (position, value) {
      $('#input-days').val(value);
    },
  });

  $('#input-days').on('input', function () {
    $('#slider-days').val($(this).val()).change();
  });

  $('.open-modal').on('click', function (e) {
    e.preventDefault();
    $('.r-modal').slideToggle('fast', function (e) {
      // callback
    });
  });

  $('.r-modal__close').on('click', function (e) {
    e.preventDefault();
    $('.r-modal').slideToggle('fast', function (e) {
      // callback
    });
  });

  $('.r-header__mob-nav').on('click', function (e) {
    $('.r-nav').slideToggle('fast', function (e) {});
    $('.r-header__info').slideToggle('fast', function (e) {});
    $('.r-header__numbers').slideToggle('fast', function (e) {});
  });

  objectFitImages();
});
